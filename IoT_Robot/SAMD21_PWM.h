#include <Wiring.h>

void setupPWM(){
  // Feed GCLK0 at 48MHz to TCC0 and TCC1
  GCLK->CLKCTRL.reg = GCLK_CLKCTRL_CLKEN |         // Enable GCLK0 as a clock source
                      GCLK_CLKCTRL_GEN_GCLK0 |     // Select GCLK0 at 48MHz
                      GCLK_CLKCTRL_ID_TCC0_TCC1;   // Feed GCLK0 to TCC0 and TCC1
  while (GCLK->STATUS.bit.SYNCBUSY);               // Wait for synchronization

  // Enable the port multiplexer for pins D7
  PORT->Group[g_APinDescription[7].ulPort].PINCFG[g_APinDescription[6].ulPin].bit.PMUXEN = 1;
  PORT->Group[g_APinDescription[7].ulPort].PINCFG[g_APinDescription[5].ulPin].bit.PMUXEN = 1;

  // D7 is on EVEN port pin PA06 and TCC1/WO[0] channel 0 is on peripheral E
  PORT->Group[g_APinDescription[7].ulPort].PMUX[g_APinDescription[6].ulPin >> 1].reg = PORT_PMUX_PMUXE_E | PORT_PMUX_PMUXE_E;
  PORT->Group[g_APinDescription[7].ulPort].PMUX[g_APinDescription[5].ulPin >> 1].reg = PORT_PMUX_PMUXO_E | PORT_PMUX_PMUXE_E;
  
  // Normal (single slope) PWM operation: timer countinuouslys count up to PER register value and then is reset to 0
  TCC0->WAVE.reg |= TCC_WAVE_WAVEGEN_NPWM;         // Setup single slope PWM on TCC1
  while (TCC1->SYNCBUSY.bit.WAVE);                 // Wait for synchronization
  
  TCC0->PER.reg = 48978;                            // Set the frequency of the PWM on TCC1 to 38kHz: 48MHz / (1262 + 1) = 38kHz
  while (TCC0->SYNCBUSY.bit.PER);                  // Wait for synchronization
  
  TCC0->CTRLA.bit.ENABLE = 1;                     // Enable the TCC1 counter
  while (TCC0->SYNCBUSY.bit.ENABLE);              // Wait for synchronization
}

void writePWM(float value) {
   // Using buffered counter compare registers (CCBx)
  TCC0->CCB[0].reg = (int) ((value/255)*48978); //315                        // TCC1 CCB1 - 25% duty cycle on D7
  while (TCC0->SYNCBUSY.bit.CCB0);                // Wait for synchronization

  TCC0->CCB[1].reg = (int) ((value/255)*48978); //315                        // TCC1 CCB1 - 25% duty cycle on D7
  while (TCC0->SYNCBUSY.bit.CCB1);                // Wait for synchronization
  
}
