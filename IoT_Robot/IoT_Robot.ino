#include <Adafruit_ZeroTimer.h>
#include <ArduinoJson.h>
#include <WiFiNINA.h>
#include <SPI.h>
#include "arduino_secrets.h"
#include "math.h"
#include "Pins.h"
#include "KalmanFilter.h"
#include <Arduino_LSM6DS3.h>
#include <Arduino.h>
#include "SAMD21_PWM.h"
#include <MQTT.h>


#define targetAngle 0
#define targetSpeed 0
#define Kpt  -55
#define Kdt  -0.75
#define Kps  -10
#define Kis  -0.26


#define BROKER_IP    "192.168.1.130"
#define DEV_NAME     "Robot"
char ssid[] = SECRET_SSID;
char pass[] = SECRET_PASS;

String sending = "closed";

Adafruit_ZeroTimer zerotimer = Adafruit_ZeroTimer(3);
void TC3_Handler() {
  Adafruit_ZeroTimer::timerHandler(3);
}


WiFiClient net;
MQTTClient client;

int status = WL_IDLE_STATUS;

StaticJsonDocument<500> doc;
KalmanFilter kalmanfilter;

volatile unsigned long encoder_count_right_a = 0;
volatile unsigned long encoder_count_left_a = 0;

float ax, ay, az, gx, gy, gz;
float accAngle,loopTime,loopTimePos,currTime,currTimePos,kalmanfilter_angle;
float prevTime = 0.0;
float prevTimePos = 0.0;
volatile int  balance_control,motorPower;
volatile float gyroAngle = 0, currentAngle, prevAngle, error, errorSum = 0, Gyro_x;
int encoder_left_pulse_num_speed = 0;
int encoder_right_pulse_num_speed = 0;
int speed_control_period_count = 0;
int speed_control;
double car_speed_integeral = 0;
double speed_filter = 0;
double speed_filter_old = 0;
int setting_car_speed = 0;
double car_speed = 0;
float dt = 0.005, Q_angle = 0.001, Q_gyro = 0.005, R_angle = 0.5, C_0 = 1, K1 = 0.05;

int count = 0;

void connect() {
 Serial.print("Checking WiFi...");
 while (WiFi.status() != WL_CONNECTED) {
   Serial.print(".");
   status = WiFi.begin(ssid, pass);
   delay(500);
   
 }
 Serial.print("\nConnecting...");
 while (!client.connect(DEV_NAME)){
   Serial.print(".");
   delay(500);
 }
 Serial.println("\nConnected!");
 client.subscribe("robot/data");
 Serial.println("Subscribed!");
}

void messageReceived(String &topic, String &payload) {
 if (count%100==0){Serial.println("incoming: " + topic + " - " + payload+ " - " + String(count%100));
 if (topic == "/hello") {
   if (payload == "open") {
     Serial.println("open");
     digitalWrite(LED_BUILTIN, HIGH); 
   } else if (payload == "closed") {
     Serial.println("closed");
     digitalWrite(LED_BUILTIN, LOW); 
   }
 }}
}

void carStop()
{
  digitalWrite(AIN1, HIGH);
  digitalWrite(BIN1, LOW);
  digitalWrite(STBY_PIN, HIGH);
  analogWrite(PWMA_LEFT, 0);
  analogWrite(PWMB_RIGHT, 0);
}

void encoderCountRightA()
{
  encoder_count_right_a++;
}


void encoderCountLeftA()
{
  encoder_count_left_a++;
}

void setup(){
  IMU.begin();
  Serial.begin(115200);
  delay(1000);
  pinMode(LED_BUILTIN, OUTPUT);
  WiFi.begin(ssid, pass);
  client.begin(BROKER_IP, 1883, net);
  client.onMessage(messageReceived);
  connect();
  printWiFiStatus();

  setupPWM();
  
  pinMode(AIN1, OUTPUT);
  pinMode(BIN1, OUTPUT);

  attachInterrupt(digitalPinToInterrupt(ENCODER_LEFT_A_PIN), encoderCountLeftA, CHANGE);
  carStop();
  
  uint8_t divider  = 8;
  uint16_t compare = (48000000/8)*0.005;
  tc_clock_prescaler prescaler = TC_CLOCK_PRESCALER_DIV8;

  zerotimer.enable(false);
  zerotimer.configure(prescaler,       // prescaler
          TC_COUNTER_SIZE_16BIT,       // bit width of timer/counter
          TC_WAVE_GENERATION_MATCH_FREQ // frequency or PWM mode
          );

  zerotimer.setCompare(0, compare);
  zerotimer.setCallback(true, TC_CALLBACK_CC_CHANNEL0, balanceCar);
  zerotimer.enable(true);

  delay(100);
}

void balanceCar() {

  if (!client.connected()){
    while (!client.connect(DEV_NAME)){}
    client.subscribe("robot/data");
   }

  encoder_left_pulse_num_speed += motorPower < 0 ? -encoder_count_left_a : encoder_count_left_a;
  //encoder_right_pulse_num_speed += motorPower < 0 ? -encoder_count_right_a : encoder_count_right_a;
  encoder_count_left_a = 0;
  //encoder_count_right_a = 0;
  
  currTime = millis();
  loopTime = currTime - prevTime;
  prevTime = currTime;

  IMU.readAcceleration(ax,ay,az);
  IMU.readGyroscope(gx,gy,gz);

  kalmanfilter.Angle(ax, ay, az, gx, gy, gz, dt, Q_angle, Q_gyro, R_angle, C_0, K1);
  currentAngle = kalmanfilter.angle;

  //accAngle = -atan2(ay, az)*RAD_TO_DEG;
  //gyroAngle = -gx*loopTime/1000; 
  //currentAngle = 0.98*(prevAngle + gyroAngle) + 0.02*(accAngle);

  balance_control = Kpt*(currentAngle - targetAngle) + Kdt * (-gx - targetSpeed);

  speed_control_period_count++;
  if (speed_control_period_count >= 8)
  {
    speed_control_period_count = 0;
    double car_speed = encoder_left_pulse_num_speed;
    speed_filter = speed_filter_old * 0.7 + car_speed * 0.3;
    speed_filter_old = speed_filter;
    car_speed_integeral += speed_filter;
    car_speed_integeral += -setting_car_speed;
    car_speed_integeral = constrain(car_speed_integeral, -3000, 3000);
    speed_control = Kps * speed_filter + Kis * car_speed_integeral;
    
    encoder_left_pulse_num_speed = 0;
    encoder_right_pulse_num_speed = 0;

  }
  
  motorPower =  - balance_control - speed_control;
  motorPower = constrain(motorPower, -255, 255);
  
  
  if (motorPower > 0) {
      digitalWrite(AIN1, 0);
      digitalWrite(BIN1, 0);
      writePWM(motorPower);
  } else {
      digitalWrite(AIN1, 1);
      digitalWrite(BIN1, 1);
      writePWM(-motorPower);
  }
  //prevAngle = currentAngle;

    doc["angle"] = currentAngle;
    doc["speed"] = 1000*speed_filter*0.0325*2*M_PI/780/loopTime/8;
    doc["motorPower"] = motorPower;
    doc["loopTime"] = loopTime/1000;
    
    String postData;
    serializeJson(doc, postData);

    client.publish("robot/data", postData);
      
}

void loop(){}

void printWiFiStatus() {
  // print the SSID of the network you're attached to:
  Serial.print("SSID: ");
  Serial.println(WiFi.SSID());

  // print your WiFi shield's IP address:
  IPAddress ip = WiFi.localIP();
  Serial.print("IP Address: ");
  Serial.println(ip);

  // print the received signal strength:
  long rssi = WiFi.RSSI();
  Serial.print("signal strength (RSSI):");
  Serial.print(rssi);
  Serial.println(" dBm");
}
