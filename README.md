# Era autonomoan orekatutako robota
Gordailu honetan Martin Graok egindako Elektronikako Graduaren GrAL-ean idatzitako kodea dago.

Kodea erabiltzeko "mosquitto.conf", "Client.ipynb" eta "IoT_Robot.ino" fitxategietan broker-aren IP-a zehaztu behar da eta, "arduino_secrets.h" fitxategiean WiFi sarearen SSID eta pasahitza.

Broker-a hasieratzeko nahikoa da hurrengo komandoa exekutatzea:
~~~
mosquitto -c mosquitto.conf
~~~

Lan hau CC BY-NC-SA 4.0 lizentziaren bidez babestua dago: https://creativecommons.org/licenses/by-nc-sa/4.0/deed.en

